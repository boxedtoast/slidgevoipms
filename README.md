# slidge voipms

A [voip.ms](https://voip.ms) to [XMPP](https://xmpp.org) puppetering 
[gateway](https://xmpp.org/extensions/xep-0100.html), based on
[slidge](https://slidge.im).

## Installation

Refer to the [slidge admin documentation](https://slidge.im/core/admin/)
for general info on how to set up an XMPP server component.

### Containers

The container is available on codeberg. See the `docker-compose-prod.yml` for an example of how to pull it in.
