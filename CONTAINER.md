# Maintainer instructions on how to update the docker container

Build the container

`docker build .`

Find the line in the output after the prod container has been built (it's the middle one after build and before dev), grab it's sha.
You can run `docker images` to check that it matches.

Tag the image

`docker tag codeberg.org/boxedtoast/slidgevoipms:latest <IMAGE ID>`

Login to codeberg (if you haven't already)

`docker login codeberg.org`

Push the image

`docker push codeberg.org/boxedtoast/slidgevoipms:latest`
