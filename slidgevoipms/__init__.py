"""
Voip ms sms/mms legacy module for slidge.
"""

from . import gateway, session

__all__ = "gateway", "session"
