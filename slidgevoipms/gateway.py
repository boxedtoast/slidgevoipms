"""
The gateway
"""

import aiohttp
from typing import Optional

from slixmpp import JID

from slidge import BaseGateway, FormField, GatewayUser
from slidge.command.register import RegistrationType
from slixmpp.exceptions import XMPPError

from .session import API_URL


class Gateway(BaseGateway):
    """
    This is instantiated once by the slidge entrypoint.

    By customizing the class attributes, we customize the registration process,
    and display name of the component.
    """

    COMPONENT_NAME = "voip.ms sms gateway"
    COMPONENT_TYPE = "aim"
    REGISTRATION_INSTRUCTIONS = (
        "Register with your voip.ms username and API password"
        "Then you will need to enter the DID you wish to use."
    )
    REGISTRATION_TYPE = RegistrationType.SINGLE_STEP_FORM
    REGISTRATION_FIELDS = [
        FormField(var="username", label="User name", required=True),
        FormField(var="password", label="Voip MS API password", required=True, private=True),
        FormField(var="did", label="Did (phone number)", required=True),
    ]
    GROUPS = False
    MARK_ALL_MESSAGES = False

    async def validate(
        self, user_jid: JID, registration_form: dict[str, Optional[str]]
    ):
        """
        This function receives the values of the form defined in
        :attr:`REGISTRATION_FIELDS`. Here, since we set
        :attr:`REGISTRATION_TYPE` to "SINGLE_STEP_FORM", if login fails
        the user will see an exception

        :param user_jid:
        :param registration_form:
        :return:
        """
        async with aiohttp.ClientSession() as session:
            async with session.get(API_URL, params={
                'api_username': registration_form.get('username'),
                'api_password': registration_form.get('password'),
                'method': 'getDIDsInfo',
                'content_type': 'json',
            }) as response:
                # For whatever cursed reason, this call sometimes returns with
                # text/html as the content type, so we disable the check
                try:
                    json = await response.json(content_type=None)
                    if json['status'] == 'success':
                        return
                    else:
                        raise XMPPError("Received error from voipms")
                except:
                    raise XMPPError("Failure decoding response from voipms")